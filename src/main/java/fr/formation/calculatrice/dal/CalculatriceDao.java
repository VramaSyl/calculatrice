package fr.formation.calculatrice.dal;

import fr.formation.calculatrice.bo.Calcul;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CalculatriceDao extends CrudRepository<Calcul,Integer> {
}
