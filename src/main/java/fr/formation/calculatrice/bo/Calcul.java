package fr.formation.calculatrice.bo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Calcul {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer a;
    private Integer b;
    private Integer res;
    
    
    public Calcul() {
    }
    
    
    public Calcul(Integer id, Integer a, Integer b, Integer res) {
        this.id = id;
        this.a = a;
        this.b = b;
        this.res = res;
    }
    
    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public Integer getA() {
        return a;
    }
    
    public void setA(Integer a) {
        this.a = a;
    }
    
    public Integer getB() {
        return b;
    }
    
    public void setB(Integer b) {
        this.b = b;
    }
    
    public Integer getRes() {
        return res;
    }
    
    public void setRes(Integer res) {
        this.res = res;
    }
    
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Calcul{");
        sb.append("id=").append(id);
        sb.append(", a=").append(a);
        sb.append(", b=").append(b);
        sb.append(", res=").append(res);
        sb.append('}');
        return sb.toString();
    }
    
    public Calcul(Integer res) {
        this.res = res;
    }
}
