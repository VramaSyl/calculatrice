package fr.formation.calculatrice.ws;



import fr.formation.calculatrice.bll.CalculatriceImpl;
import fr.formation.calculatrice.bo.Calcul;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalculatriceController {
    
    @Autowired
    CalculatriceImpl calculatrice;
    
   @RequestMapping("/home")
   public String affiche(){
       String str="hello";
       return str;
   }
   
    @GetMapping("/add")
    public Integer addition(@RequestParam Integer a , @RequestParam Integer b) {
       Integer res=calculatrice.addition(a, b);
       return res;
    }
    
    @GetMapping("/sous")
    public Integer soustraction(@RequestParam Integer a, @RequestParam Integer b){
       Integer res=calculatrice.soustraction(a,b);
       return res;
    }
}
