package fr.formation.calculatrice.bll;


import fr.formation.calculatrice.bo.Calcul;
import fr.formation.calculatrice.dal.CalculatriceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.LinkedList;
import java.util.List;

@Service
public class CalculatriceImpl implements Calculatrice {
    
    @Autowired
    CalculatriceDao dao;
    
    @Override
    public Integer addition(Integer a, Integer b) {
        Calcul c = new Calcul();
        c.setA(a);
        c.setB(b);
        c.setRes(c.getA() + c.getB());
        dao.save(c);
        return c.getRes();
    }
    
    @Override
    public Integer soustraction(Integer a, Integer b) {
        Calcul c = new Calcul();
        c.setA(a);
        c.setB(b);
        c.setRes(c.getA() - c.getB());
        dao.save(c);
        return c.getRes();
    }
    
    @Override
    public List<Calcul> findAll() {
        return (List<Calcul>) dao.findAll();
        
        
    }
    
    
}
