package fr.formation.calculatrice.bll;

import fr.formation.calculatrice.bo.Calcul;

public interface Calculatrice {
    public  Integer addition(Integer a, Integer b);
    public Integer soustraction(Integer a,Integer b);
    public Iterable<Calcul> findAll();
}
